//
// Created by Bastien on 09.03.2022.
//

#include "Point.h"

Point::Point():coordinate_x(0), coordinate_y(0) {
    Point::coordinate_x = 0;
    Point::coordinate_y = 0;
}

Point::Point(int coordinate_x, int coordinate_y):coordinate_x(coordinate_x), coordinate_y(coordinate_y) {
    Point::coordinate_x = coordinate_x;
    Point::coordinate_y = coordinate_y;
}

Point::Point(const Point& other):coordinate_x(other.coordinate_x), coordinate_y(other.coordinate_y){
    // copy constructor
}

Point& Point::operator=(Point other){
    // affectation operator
    this->coordinate_x = other.coordinate_x;
    this->coordinate_y = other.coordinate_y;
    return *this;
}

Point::~Point(){}

int Point::getCoordinateX(){
    return coordinate_x;
}

void Point::setCoordinateX(int coordinateX) {
    coordinate_x = coordinateX;
}

int Point::getCoordinateY(){
    return coordinate_y;
}

void Point::setCoordinateY(int coordinateY) {
    coordinate_y = coordinateY;
}
