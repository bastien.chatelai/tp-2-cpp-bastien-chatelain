//
// Created by Bastien on 09.03.2022.
//

#include "Picture.h"
#include <fstream>
#include <iostream>
#include "Forms/Square.h"
#include <bits/stdc++.h>
#include "Forms/Rectangle.h"

// https://stackoverflow.com/questions/36098367/sorting-vectors-of-shared-pointers-in-c


Picture::Picture() {}

Picture::Picture(int pictureHeight, int pictureWidth) : picture_height(pictureHeight), picture_width(pictureWidth) {
    this->picture_height = pictureHeight;
    this->picture_width = pictureWidth;
}

Picture::Picture(const Picture& other):picture_height(other.picture_height), picture_width(other.picture_width), form_list(other.form_list){
    // copy constructor
}
Picture& Picture::operator=(Picture other){
    // affectation operator
    this->setPictureHeight(other.picture_height);
    this->setPictureWidth(other.picture_width);
    return *this;
}

Picture::~Picture() {}

int Picture::getPictureHeight(){
    return picture_height;
}

void Picture::setPictureHeight(int pictureHeight) {
    picture_height = pictureHeight;
}

int Picture::getPictureWidth(){
    return picture_width;
}

void Picture::setPictureWidth(int pictureWidth) {
    picture_width = pictureWidth;
}

std::vector<std::shared_ptr<Form>> Picture::getFormList(){
    return form_list;
}

void Picture::setFormList(std::vector<std::shared_ptr<Form>> formList){
    form_list = formList;
}

void Picture::addForm(std::shared_ptr<Form> form) {
    form_list.push_back(form);
}

void Picture::printPictureInFile(const char *output_filename) {
    std::ofstream output_file;
    output_file.open(output_filename);

    std::vector<char> line_to_print (picture_width, ' ');
    line_to_print.push_back('\n');

    for (int line = 0; line < picture_height; line++){
        std::fill(line_to_print.begin(), line_to_print.end() - 1, ' ');
        for (std::shared_ptr<Form> form : this->getFormList()){
            form->getLineToPrintAtCoordinateY(line_to_print,line, picture_width);
        }

        for (char c : line_to_print){
            output_file << c;
        }

    }

    output_file.close();

}

