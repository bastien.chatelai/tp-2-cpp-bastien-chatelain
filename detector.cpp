#include <iostream>
#include "Point.h"
#include "Picture.h"
#include <memory>
#include <fstream>
#include "Forms/Rectangle.h"
//#include <string>

int main() {

    const char* input_file_name = "./data/output.txt";
    const char* output_file_name = "./data/output_cmd.txt";

    std::ifstream input_file;
    input_file.open(input_file_name);


    //std::cout << input_file.is_open() << std::endl;
    std::string line;


    // get image width and height
    int picture_height = 0;
    int picture_width = 0;
    while (std::getline(input_file, line)){
        if (picture_width == 0){
            picture_width = line.length();
        }
        picture_height++;
    }


    input_file.close();

    Picture picture(picture_height,picture_width);
    auto form = std::shared_ptr<Form>();
    std::ofstream output_file;
    output_file.open(output_file_name);
    input_file.open(input_file_name);
    int height_index = 0;
    while(std::getline(input_file, line)){
        int rectangle_x_coordinate = -1;
        int rectangle_width = 0;
        for(int i = 0; i < (int)line.length(); i++){
            if (rectangle_x_coordinate != -1){
                if (line.at(i) == '#'){ // # was previously detected
                    rectangle_width++;
                } else { // create the rectangle
                    form = std::unique_ptr<Form>(new Rectangle(Point(rectangle_x_coordinate, height_index), 1, rectangle_width));
                    picture.addForm(form);
                    //std::cout << "Rectangle;" << rectangle_x_coordinate << ";" << height_index << ";1;" << rectangle_width << std::endl;
                    output_file << "Rectangle;" << rectangle_x_coordinate << ";" << height_index << ";1;" << rectangle_width << std::endl;
                    rectangle_x_coordinate = -1;
                    rectangle_width = 0;
                }
            } else {
                if (line.at(i) == '#'){ // # is detected, begin of a rectangle
                    rectangle_x_coordinate = i;
                    rectangle_width++;
                }
            }

        }
        height_index++;
    }

    input_file.close();
    output_file.close();

    return 0;
}
