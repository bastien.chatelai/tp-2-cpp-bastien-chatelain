#include <iostream>
#include "Point.h"
#include "Forms/Square.h"
#include "Picture.h"
#include "Forms/Rectangle.h"
#include <memory>
#include <random>

int main() {

    const char* file_name = "./data/output.txt";
    const int picture_height = 1000;
    const int picture_width = 1000;
    const int min_number_of_generated_form = 50;
    const int max_number_of_generated_form = 500;
    const int number_type_of_form = 2;

    Picture picture(picture_height,picture_width);

    std::random_device rd;
    std::default_random_engine eng(rd());
    std::uniform_int_distribution<int> distr_choose_form(0, number_type_of_form);
    std::uniform_int_distribution<int> distr_choose_coordinate_y(0, picture_height);
    std::uniform_int_distribution<int> distr_choose_coordinate_x(0, picture_width);
    std::uniform_int_distribution<int> distr_choose_side_size(1, picture_width/5);
    std::uniform_int_distribution<int> distr_choose_number_of_form(min_number_of_generated_form, max_number_of_generated_form);
    auto form = std::shared_ptr<Form>();

    for (int i = 0; i < distr_choose_number_of_form(eng); i++){
        switch(distr_choose_form(eng)){
            case 0:
                form = std::shared_ptr<Form>(new Rectangle(Point(distr_choose_coordinate_x(eng),distr_choose_coordinate_y(eng)), distr_choose_side_size(eng), distr_choose_side_size(eng)));
                picture.addForm(form);
                break;
            case 1:
                form = std::shared_ptr<Form>(new Square(Point(distr_choose_coordinate_x(eng), distr_choose_coordinate_y(eng)), distr_choose_side_size(eng)));
                picture.addForm(form);
                break;
            default:
                break;
        }
    }

    picture.printPictureInFile(file_name);


    return 0;
}
