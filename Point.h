//
// Created by Bastien on 09.03.2022.
//

#ifndef TP_2_CPP_BASTIEN_CHATELAIN_POINT_H
#define TP_2_CPP_BASTIEN_CHATELAIN_POINT_H


class Point {
public:
    Point();
    Point(int coordinate_x, int coordinate_y);
    Point(const Point& other);
    Point& operator=(Point other);
    ~Point();

    int getCoordinateX();

    void setCoordinateX(int coordinateX);

    int getCoordinateY();

    void setCoordinateY(int coordinateY);

private:
    int coordinate_x;
    int coordinate_y;

};


#endif //TP_2_CPP_BASTIEN_CHATELAIN_POINT_H
