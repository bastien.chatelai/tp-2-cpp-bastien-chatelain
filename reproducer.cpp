//
// Created by Bastien on 18.03.2022.
//

#include <fstream>
#include <sstream>
#include <iostream>
#include <vector>
#include "Picture.h"
#include "Forms/Square.h"
#include "Forms/Rectangle.h"
#include "Point.h"

int main(){
    const char* input_commands_file_name = "./data/output_cmd.txt";
    const int picture_width = 1000;
    const int picture_height = 1000;

    Picture picture(picture_height, picture_width);

    std::ifstream input_commands_file;
    input_commands_file.open(input_commands_file_name);

    std::string line;
    auto form = std::shared_ptr<Form>();


    while(std::getline(input_commands_file, line)){
        std::istringstream iss(line);
        std::vector<std::string> tokens;
        std::string token;
        while(std::getline(iss, token, ';')){
            tokens.push_back(token);
        }

        if (tokens.at(0) == "Rectangle"){
            form = std::shared_ptr<Form>(new Rectangle(Point(stoi(tokens.at(1)), stoi(tokens.at(2))), stoi(tokens.at(3)), stoi(tokens.at(4))));
            picture.addForm(form);
        } else if (tokens.at(0) == "Square"){
            form = std::shared_ptr<Form>(new Square(Point(stoi(tokens.at(1)), stoi(tokens.at(2))), stoi(tokens.at(3))));
            picture.addForm(form);
        } else {
            // error : not form detected
        }

    }

    input_commands_file.close();

    const char* output_file_name = "./data/output_reproduced.txt";
    picture.printPictureInFile(output_file_name);

    return 0;
}