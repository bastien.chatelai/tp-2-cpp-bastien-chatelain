//
// Created by Bastien on 09.03.2022.
//

#ifndef TP_2_CPP_BASTIEN_CHATELAIN_PICTURE_H
#define TP_2_CPP_BASTIEN_CHATELAIN_PICTURE_H

#include <vector>
#include <array>
#include <memory>
#include "Forms/Form.h"

class Picture {
public:
    Picture();

    Picture(int pictureHeight, int pictureWidth);
    Picture(const Picture& other);
    Picture& operator=(Picture other);
    ~Picture();

    int getPictureHeight();

    void setPictureHeight(int pictureHeight);

    int getPictureWidth();

    void setPictureWidth(int pictureWidth);

    std::vector<std::shared_ptr<Form>> getFormList();

    void setFormList(std::vector<std::shared_ptr<Form>> formList);

    void addForm(std::shared_ptr<Form> form);

    void printPictureInFile(const char* output_filename);


private:
    int picture_height;
    int picture_width;
    std::vector<std::shared_ptr<Form>> form_list;

};


#endif //TP_2_CPP_BASTIEN_CHATELAIN_PICTURE_H
