//
// Created by Bastien on 09.03.2022.
//

#ifndef TP_2_CPP_BASTIEN_CHATELAIN_FORM_H
#define TP_2_CPP_BASTIEN_CHATELAIN_FORM_H
#include "../Point.h"
#include <vector>

class Form {
public:
    Form();
    Form(Point form_location);
    Form(const Form& other);
    Form& operator=(void* param);
    ~Form();

    Point &getFormLocation();

    void setFormLocation(Point &formLocation);
    virtual void getLineToPrintAtCoordinateY(std::vector<char> &line_to_print,int coordonate_y, int picture_width) = 0;

private:
    Point form_location;
};


#endif //TP_2_CPP_BASTIEN_CHATELAIN_FORM_H
