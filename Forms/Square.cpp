//
// Created by Bastien on 09.03.2022.
//

#include "Square.h"
#include <vector>
#include <iostream>

Square::Square() {
    side = 1;
}


Square::Square(const Point &formLocation, int side) : Form(formLocation), side(side) {
    this->side = side;
}

Square::Square(const Square &other) : side(other.side) {
    // copy constructor
}

Square& Square::operator=(Square other) {
    this->side = other.side;
    this->setFormLocation(other.getFormLocation());
    return *this;
}


Square::~Square() {

}

int Square::getSide() {
    return side;
}

void Square::setSide(int side) {
    this->side = side;
}

void Square::getLineToPrintAtCoordinateY(std::vector<char> &line_to_print,int coordonate_y, int picture_width){
    int form_coordinate_y = this->getFormLocation().getCoordinateY();
    int form_coordinate_x = this->getFormLocation().getCoordinateX();

    if ((coordonate_y >= form_coordinate_y) && (coordonate_y < form_coordinate_y + side)){
        for (int i = form_coordinate_x; i < form_coordinate_x + side; i++){
            if (i <= picture_width-1){
                line_to_print.at(i) = '#';
            }

        }
    }

}
