//
// Created by Bastien on 09.03.2022.
//

#include "Rectangle.h"
#include <iostream>

Rectangle::Rectangle() {
    height = 1;
    width = 2;
}
Rectangle::~Rectangle() {};

Rectangle::Rectangle(const Point &formLocation) : Form(formLocation) {
    height = 1;
    width = 2;
}

Rectangle::Rectangle(const Point &formLocation, int height, int width) : Form(formLocation), height(height),
                                                                         width(width) {
    this->height = height;
    this->width = width;
}

Rectangle::Rectangle(const Rectangle &other): height(other.height), width(other.width){
    // copy constructor
}

Rectangle& Rectangle::operator=(Rectangle other) {
    this->height = other.height;
    this->width = other.width;
    this->setFormLocation(other.getFormLocation());
    return *this;
}

int Rectangle::getHeight() {
    return height;
}

void Rectangle::setHeight(int height) {
    Rectangle::height = height;
}

int Rectangle::getWidth() {
    return width;
}

void Rectangle::setWidth(int width) {
    Rectangle::width = width;
}

void Rectangle::getLineToPrintAtCoordinateY(std::vector<char> &line_to_print, int coordonate_y, int picture_width) {
    int form_coordinate_y = this->getFormLocation().getCoordinateY();
    int form_coordinate_x = this->getFormLocation().getCoordinateX();

    if ((coordonate_y >= form_coordinate_y) && (coordonate_y < form_coordinate_y + height)){
        for (int i = form_coordinate_x; i < form_coordinate_x + width; i++){
            if (i <= picture_width-1){
                line_to_print.at(i) = '#';
            }

        }
    }

}


