//
// Created by Bastien on 09.03.2022.
//

#ifndef TP_2_CPP_BASTIEN_CHATELAIN_SQUARE_H
#define TP_2_CPP_BASTIEN_CHATELAIN_SQUARE_H

#include "Form.h"

class Square: public Form {
public:
    Square();

    Square(const Point &formLocation, int side);
    Square(const Square& other);
    Square& operator=(Square other);
    ~Square();

    int getSide();

    void setSide(int squareSide);

    void getLineToPrintAtCoordinateY(std::vector<char> &line_to_print,int coordonate_y, int picture_width) override;

private:
    int side;
};


#endif //TP_2_CPP_BASTIEN_CHATELAIN_SQUARE_H
