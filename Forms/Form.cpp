//
// Created by Bastien on 09.03.2022.
//

#include "Form.h"
#include <iostream>

Form::Form() {
    form_location = Point(0,0);
}

Form::~Form(){}

Form::Form(Point form_location):form_location(form_location.getCoordinateX(), form_location.getCoordinateY()){
    this->form_location = form_location;
}

Form::Form(const Form &other):form_location(other.form_location){
    // copy constructor
}

Form& Form::operator=(void* param) {

    return *this;
}


Point &Form::getFormLocation() {
    return form_location;
}

void Form::setFormLocation(Point &formLocation) {
    form_location = formLocation;
}

