//
// Created by Bastien on 09.03.2022.
//

#ifndef TP_2_CPP_BASTIEN_CHATELAIN_RECTANGLE_H
#define TP_2_CPP_BASTIEN_CHATELAIN_RECTANGLE_H

#include "Form.h"

class Rectangle : public Form{
public:
    Rectangle(const Point &formLocation);
    Rectangle();
    Rectangle(const Rectangle& other);

    Rectangle(const Point &formLocation, int height, int width);

    Rectangle& operator=(Rectangle other);

    ~Rectangle();

    int getHeight();

    void setHeight(int height);

    int getWidth();

    void setWidth(int width);

    void getLineToPrintAtCoordinateY(std::vector<char> &line_to_print, int coordonate_y, int picture_width) override;


private :
    int height;
    int width;
};


#endif //TP_2_CPP_BASTIEN_CHATELAIN_RECTANGLE_H
